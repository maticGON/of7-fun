# OF7-FUN

## How to build and run application
1. Checkout code from repository.
2. Navigate to checkout destination and search for `pom.xml` file.
3. Run command `mvn clean install` from directory with `pom.xml`.
4. After successful completion `target` directory will appear in same checkout directory.
5. Search for `Fun7Server6-1.0-SNAPSHOT.war` file in `target` directory.
6. Move file to your tomcat container's directory `webapps`. Optionally rename .war file to `Fun7Server.war`.
7. Navigate to `tomcat/bin` and run command `catalina.out start`.
8. After successful startup of tomcat use preferred REST/API client to execute following requests:
9. Status of all available features: `<ip>:<port>/<app_name>/featuresStatus?timezone=America/New_York&userId=TestUser&cc=US`
10. Play a game: `<ip>:<port>/<war_name>/play1Game?timezone=America/New_York&userId=TestUser&cc=US`
11. Example: `localhost:8080/Fun7Server6/play1Game?timezone=America/New_York&userId=TestUser&cc=US` (no .war in app name)
## Assumptions
1. Multiplayer status under `/featuresStatus` changes after you (successfully) call `/play1Game` more then 5 times.
2. Ads Server is randomly returning positive and negative responses, sometimes even errors. Enabled state is shown only when positive response is received from 3rd party server otherwise feature gets disabled.
3. Project is missing logging solution, so there are some `System.out.println` in code.
4. There are empty implementations of all features listed in requirements paper. They all extend `AbstractFeature class`, which makes adding new features to the system trivial. 
5. Javadocs should be on all non-trivial methods or on their interface declarations.
6. Few unit/integration tests are under `@Ignore` flag, because Ads Server is returning random responses.
7. Unit tests are covering almost 100% of business logic (except DTOs, main classes, constructors,...)
8. Some TODOs are left in project to show what could be done in the future or is just prepared to be upgraded if needed.
9. There is not Google App Engine integration nor database has been added. Instead simple `MemoryDB class` which implements `IPersistence interface` has been added. It caches data in HashMap. System is build in a way that changing DB is simple, because new concrete DB implementation just have to implement `IPersistence` interface (same concept as `IFeature` that is implemented by `AbstracFeature` which all features should extend.)
10. Some features are over-engineered, some implemented too generic for specified example (e.g. `RestClient class`), some are too simple (e.g. `App Class`), some are missing (e.g. logging, configuration load at startup for constants [max played games limit, user/password for ads server, ...]). Hopefully used concepts can be recognized.