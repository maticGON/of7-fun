import com.fun7.app.FeatureRegister;
import com.fun7.features.IFeature;
import com.fun7.features.ads.AdsFeature;
import com.fun7.features.cs.CustomerSupportFeature;
import com.fun7.features.multiplayer.MultiplayerFeature;
import com.fun7.persistence.impl.MemoryDB;
import com.fun7.rest.RestService;

import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Set;

import javax.ws.rs.core.Response;

public class IntegartionTest
{
    private static Dispatcher dispatcher;
    private static POJOResourceFactory noDefaults;

    @BeforeClass
    public static void setup ()
    {
        dispatcher = MockDispatcherFactory.createDispatcher();
        noDefaults = new POJOResourceFactory(RestService.class);
        dispatcher.getRegistry().addResourceFactory(noDefaults);
    }

    @Before
    public void cleanup ()
    {
        Set<Object> singletons = FeatureRegister.getInstance().getSingletons();

        for (Object singleton : new ArrayList<Object>(singletons))
        {
            FeatureRegister.getInstance().unregister(singleton);
        }
    }

    @Test
    public void helloTest ()
    {
        try
        {

            MockHttpRequest request = MockHttpRequest.get("/hello");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals("hello", response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void playGameTest () throws Exception
    {
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        try
        {

            MockHttpRequest request = MockHttpRequest.post(
                    "/play1Game?timezone=America/New_York&userId=TestUser&cc" + "=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals("{\"status\":\"game\",\"message\":\"Played\"}", response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void playGameMissingParameterUserIDTest () throws Exception
    {
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        try
        {

            MockHttpRequest request = MockHttpRequest.post("/play1Game?timezone=America/NewYor&userId=&cc" + "=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals("{\"status\":\"Error\",\"message\":\"ERROR: \\u003cUserID\\u003e value is " +
                    "missing!\"}", response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void playGameMissingParameterCCTest () throws Exception
    {
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        try
        {

            MockHttpRequest request = MockHttpRequest.post("/play1Game?timezone=America/NewYork&userId=TestUser");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals("{\"status\":\"Error\",\"message\":\"ERROR: \\u003cCountryCode\\u003e value is " +
                    "missing!\"}", response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void playGameNoFeatureRegisteredTest () throws Exception
    {
        try
        {

            MockHttpRequest request = MockHttpRequest.post(
                    "/play1Game?timezone=America/NewYor&userId=UserTest&cc" + "=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

            // Check that the message we receive is
            Assert.assertEquals("{\"status\":\"Error\",\"message\":\"ERROR: Something went wrong!\"}",
                    response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }


    @Test
    //TODO: 3rd party system randomly return positive or negative response
    public void featuresStatusTest () throws Exception
    {
        IFeature adsFeature = new AdsFeature(FeatureRegister.IS_AVAILABLE);
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        IFeature customerSupportFeature = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(adsFeature);
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        FeatureRegister.getInstance().addSingleton(customerSupportFeature);

        try
        {
            MockHttpRequest request = MockHttpRequest.get("/featuresStatus?timezone=America/New_York&userId=TestUser" +
                    "&cc=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals(("{\"ads\":\"disabled\",\"mutliplayer\":\"disabled\",\"user-support\":" +
                    "\"disabled\"}"), response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void featuresStatusMissingParameterTimezoneTest () throws Exception
    {
        IFeature adsFeature = new AdsFeature(FeatureRegister.IS_AVAILABLE);
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        IFeature customerSupportFeature = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(adsFeature);
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        FeatureRegister.getInstance().addSingleton(customerSupportFeature);

        try
        {

            MockHttpRequest request = MockHttpRequest.get("/featuresStatus?timezone=&userId=TestUser" + "&cc=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals(("{\"status\":\"Error\",\"message\":\"ERROR: \\u003cTimezone\\u003e value is " +
                    "missing!\"}"), response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void featuresStatusMissingParameterUserIdTest () throws Exception
    {
        IFeature adsFeature = new AdsFeature(FeatureRegister.IS_AVAILABLE);
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        IFeature customerSupportFeature = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(adsFeature);
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        FeatureRegister.getInstance().addSingleton(customerSupportFeature);

        try
        {

            MockHttpRequest request = MockHttpRequest.get(
                    "/featuresStatus?timezone=America/New_York&userId=" + "&cc=US");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals(("{\"status\":\"Error\",\"message\":\"ERROR: \\u003cUserID\\u003e value is " +
                    "missing!\"}"), response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void featuresStatusMissingParameterCCTest () throws Exception
    {
        IFeature adsFeature = new AdsFeature(FeatureRegister.IS_AVAILABLE);
        IFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        IFeature customerSupportFeature = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(adsFeature);
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        FeatureRegister.getInstance().addSingleton(customerSupportFeature);

        try
        {

            MockHttpRequest request = MockHttpRequest.get("/featuresStatus?timezone=America/New_York&userId=TestUser");
            MockHttpResponse response = new MockHttpResponse();
            // Invoke the request
            dispatcher.invoke(request, response);
            // Check the status code
            Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());

            // Check that the message we receive is 
            Assert.assertEquals(("{\"status\":\"Error\",\"message\":\"ERROR: \\u003cCountryCode\\u003e value is " +
                    "missing!\"}"), response.getContentAsString());
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
    }
}