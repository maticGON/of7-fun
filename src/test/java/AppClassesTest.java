import com.fun7.app.FeatureRegister;
import com.fun7.features.IFeature;
import com.fun7.features.cs.CustomerSupportFeature;
import com.fun7.features.multiplayer.IMultiplayerFeature;
import com.fun7.features.multiplayer.MultiplayerFeature;
import com.fun7.persistence.IPersistence;
import com.fun7.persistence.impl.MemoryDB;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Set;

import static org.mockito.Mockito.mock;

public class AppClassesTest
{
    @Before
    public void cleanup ()
    {
        Set<Object> singletons = FeatureRegister.getInstance().getSingletons();

        for(Object singleton : new ArrayList<Object>(singletons))
        {
            FeatureRegister.getInstance().unregister(singleton);
        }
    }

    @Test
    public void addSingletonsTest ()
    {
        IFeature feature = mock(CustomerSupportFeature.class);
        Object object = mock(Object.class);
        IPersistence db = mock(MemoryDB.class);

        try
        {
            FeatureRegister.getInstance().addSingleton(feature);
            FeatureRegister.getInstance().addSingleton(object);
            FeatureRegister.getInstance().addSingleton(db);
        } catch (Exception e)
        {
            Assert.fail();
        }
    }

    @Test
    public void iPersistenceTest () throws Exception
    {
        Assert.assertEquals(null, FeatureRegister.getInstance().getPersistence());

        IFeature feature = mock(CustomerSupportFeature.class);
        Object object = mock(Object.class);
        IPersistence db = mock(MemoryDB.class);

        FeatureRegister.getInstance().addSingleton(feature);
        FeatureRegister.getInstance().addSingleton(object);
        FeatureRegister.getInstance().addSingleton(db);

        Assert.assertEquals(db, FeatureRegister.getInstance().getPersistence());
    }

    @Test(expected = Exception.class)
    public void iPersistenceDoubleRegisterTest () throws Exception
    {
        IFeature feature = mock(CustomerSupportFeature.class);
        Object object = mock(Object.class);
        IPersistence db = mock(MemoryDB.class);
        IPersistence db2 = mock(MemoryDB.class);

        FeatureRegister.getInstance().addSingleton(feature);
        FeatureRegister.getInstance().addSingleton(object);
        FeatureRegister.getInstance().addSingleton(db);
        FeatureRegister.getInstance().addSingleton(db2);
    }

    @Test
    public void getFeatureTest () throws Exception
    {
        Assert.assertNull(FeatureRegister.getInstance().getFeature("doesNotExist"));

        IPersistence db = new MemoryDB();
        IFeature support = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        IFeature multiplayer = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, db);
        Object object = mock(Object.class);

        FeatureRegister.getInstance().addSingleton(support);
        FeatureRegister.getInstance().addSingleton(object);
        FeatureRegister.getInstance().addSingleton(db);
        FeatureRegister.getInstance().addSingleton(multiplayer);

        Assert.assertEquals(multiplayer, FeatureRegister.getInstance().getFeature(IMultiplayerFeature.FEATURE_NAME));

    }
    @Test
    public void getAllSingletonsFeatureRegisterTest () throws Exception
    {
        Assert.assertEquals(0, FeatureRegister.getInstance().getSingletons().size());


        IPersistence db = mock(MemoryDB.class);
        IFeature support = mock(CustomerSupportFeature.class);
        IFeature multiplayer = mock(MultiplayerFeature.class);
        Object object = mock(Object.class);

        FeatureRegister.getInstance().addSingleton(support);
        FeatureRegister.getInstance().addSingleton(object);
        FeatureRegister.getInstance().addSingleton(db);
        FeatureRegister.getInstance().addSingleton(multiplayer);

        Assert.assertEquals(4, FeatureRegister.getInstance().getSingletons().size());
    }
    @Test
    public void unregisterTest () throws Exception
    {
        IPersistence db = mock(MemoryDB.class);
        IFeature support = mock(CustomerSupportFeature.class);
        IFeature multiplayer = mock(MultiplayerFeature.class);
        Object object = mock(Object.class);

        FeatureRegister.getInstance().addSingleton(support);
        FeatureRegister.getInstance().addSingleton(object);
        FeatureRegister.getInstance().addSingleton(db);
        FeatureRegister.getInstance().addSingleton(multiplayer);

        FeatureRegister.getInstance().unregister(db);

        Assert.assertEquals(3, FeatureRegister.getInstance().getSingletons().size());

    }

}
