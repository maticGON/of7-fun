import com.fun7.app.FeatureRegister;
import com.fun7.features.IFeature;
import com.fun7.features.ads.AdsFeature;
import com.fun7.features.ads.IAdsFeature;
import com.fun7.features.cs.CustomerSupportFeature;
import com.fun7.features.cs.ICustomerSupportFeature;
import com.fun7.features.multiplayer.IMultiplayerFeature;
import com.fun7.features.multiplayer.MultiplayerFeature;
import com.fun7.persistence.impl.MemoryDB;
import com.fun7.rest.RestService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static com.fun7.features.cs.ICustomerSupportFeature.WORK_HOURS_END_HOURS;
import static com.fun7.features.cs.ICustomerSupportFeature.WORK_HOURS_END_MINUTES;
import static com.fun7.features.cs.ICustomerSupportFeature.WORK_HOURS_START_HOURS;
import static com.fun7.features.cs.ICustomerSupportFeature.WORK_HOURS_START_MINUTES;

public class RestTest
{
    @Before
    public void cleanup ()
    {
        Set<Object> singletons = FeatureRegister.getInstance().getSingletons();

        for(Object singleton : new ArrayList<Object>(singletons))
        {
            FeatureRegister.getInstance().unregister(singleton);
        }
    }

    @Test
    @Ignore //TODO: 3rd party system randomly return positive or negative response
    public void serverRequestAdsTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();
        IFeature adsFeature = new AdsFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(adsFeature);

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checksAdsAvailability", HashMap.class, String.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap, "US");

        Assert.assertEquals(resultMap.get(IAdsFeature.FEATURE_NAME_SHORT), RestService.FEATURE_ENABLED);
    }

    @Test
    @Ignore //Test is on ignore, because it can not be tested without if-statement or system time changing.
    public void serverRequestCustomerSupportTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();
        IFeature customerSupportFeature = new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE);
        FeatureRegister.getInstance().addSingleton(customerSupportFeature);

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checkCustomerSupportAvailability", HashMap.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap);

        LocalTime startTime = LocalTime.of(WORK_HOURS_START_HOURS, WORK_HOURS_START_MINUTES);
        LocalTime endTime = LocalTime.of(WORK_HOURS_END_HOURS, WORK_HOURS_END_MINUTES);
        LocalDateTime currentDateTime = LocalDateTime.now();
        if (currentDateTime.toLocalTime().isAfter(startTime) && currentDateTime.toLocalTime().isBefore(endTime))
        {
            Assert.assertEquals(resultMap.get(ICustomerSupportFeature.FEATURE_NAME_SHORT), RestService.FEATURE_ENABLED);
        } else
        {
            Assert.assertEquals(resultMap.get(ICustomerSupportFeature.FEATURE_NAME_SHORT),
                    RestService.FEATURE_DISABLED);
        }
    }

    @Test
    public void serverRequestMultiplayerTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();
        IMultiplayerFeature multiplayerFeature = new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, new MemoryDB());
        FeatureRegister.getInstance().addSingleton(multiplayerFeature);
        /* Play game 5 times - requirements limit */
        multiplayerFeature.playAGame("userTest");
        multiplayerFeature.playAGame("userTest");
        multiplayerFeature.playAGame("userTest");
        multiplayerFeature.playAGame("userTest");
        multiplayerFeature.playAGame("userTest");

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checkMultiplayerAvailability", HashMap.class,
                String.class, String.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap, "userTest", "US");

        Assert.assertEquals(resultMap.get(IMultiplayerFeature.FEATURE_NAME_SHORT), RestService.FEATURE_DISABLED);

        //PLAY another game to get feature enabled.
        multiplayerFeature.playAGame("userTest");

        method = RestService.class.getDeclaredMethod("checkMultiplayerAvailability", HashMap.class, String.class,
                String.class);
        method.setAccessible(true);

        resultMap = (HashMap<String, String>) method.invoke(restService, featureNameToAvailabilityMap, "userTest",
                "US");

        Assert.assertEquals(resultMap.get(IMultiplayerFeature.FEATURE_NAME_SHORT), RestService.FEATURE_ENABLED);

        //TEST if user i snot from US
        method = RestService.class.getDeclaredMethod("checkMultiplayerAvailability", HashMap.class, String.class,
                String.class);
        method.setAccessible(true);

        resultMap = (HashMap<String, String>) method.invoke(restService, featureNameToAvailabilityMap, "userTest",
                "ŠĐ");

        Assert.assertEquals(resultMap.get(IMultiplayerFeature.FEATURE_NAME_SHORT), RestService.FEATURE_DISABLED);
    }

    @Test
    public void serverRequestAdsNoFeatureRegisteredTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checksAdsAvailability", HashMap.class, String.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap, "US");

        Assert.assertEquals(resultMap.get(IAdsFeature.FEATURE_NAME_SHORT), RestService.FEATURE_DISABLED);
    }

    @Test
    public void serverRequestMultiplayerNoFeatureRegisteredTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checkMultiplayerAvailability", HashMap.class,
                String.class, String.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap, "userTest", "US");

        Assert.assertEquals(resultMap.get(IMultiplayerFeature.FEATURE_NAME_SHORT), RestService.FEATURE_DISABLED);
    }

    @Test
    public void serverRequestCustomerSupportNoFeatureRegisteredTest () throws Exception
    {
        /*Test preparation. */
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();

        RestService restService = new RestService();
        Method method = RestService.class.getDeclaredMethod("checkCustomerSupportAvailability", HashMap.class);
        method.setAccessible(true);

        HashMap<String, String> resultMap = (HashMap<String, String>) method.invoke(restService,
                featureNameToAvailabilityMap);
        Assert.assertEquals(resultMap.get(ICustomerSupportFeature.FEATURE_NAME_SHORT), RestService.FEATURE_DISABLED);
    }

}
