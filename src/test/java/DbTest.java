import com.fun7.persistence.IPersistence;
import com.fun7.persistence.impl.MemoryDB;

import org.junit.Assert;
import org.junit.Test;

public class DbTest
{
    @Test
    public void increaseGamesPlayedTest()
    {
        IPersistence db = new MemoryDB();

        int gamesPlayedCounter = 0;
        gamesPlayedCounter = db.updateMultiplayerCounter("test1");
        Assert.assertEquals(gamesPlayedCounter, 1);

        db.updateMultiplayerCounter("test1");
        gamesPlayedCounter = db.updateMultiplayerCounter("test1");

        Assert.assertEquals(gamesPlayedCounter, 3);

    }

    @Test
    public void getGamesPlayedTest()
    {
        IPersistence db = new MemoryDB();

        int gamesPlayedPlayer1Counter = 0;
        int gamesPlayedPlayer2Counter = 0;

        db.updateMultiplayerCounter("test1");
        db.updateMultiplayerCounter("test1");
        gamesPlayedPlayer1Counter = db.updateMultiplayerCounter("test1");
        gamesPlayedPlayer2Counter = db.updateMultiplayerCounter("test2");

        Assert.assertEquals(gamesPlayedPlayer1Counter, 3);
        Assert.assertEquals(gamesPlayedPlayer2Counter, 1);
        Assert.assertEquals(db.getMutliplayerCounter("test1"), gamesPlayedPlayer1Counter);
        Assert.assertEquals(db.getMutliplayerCounter("test2"), gamesPlayedPlayer2Counter);

        Assert.assertEquals(db.getMutliplayerCounter("doesNotExist"), 0);
        Assert.assertEquals(db.getMutliplayerCounter(null), 0);

    }
}
