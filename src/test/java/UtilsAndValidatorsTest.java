import com.fun7.Utils;
import com.fun7.persistence.impl.MemoryDB;
import com.fun7.rest.ParamValidator;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class UtilsAndValidatorsTest
{
    @Test
    public void timeZoneTest()
    {
        Assert.assertTrue(ParamValidator.isTimezoneValid("America/New_York"));
        Assert.assertTrue(ParamValidator.isTimezoneValid("PST"));//deprecated in java
    }

    @Test ( expected = IllegalArgumentException.class)
    public void timeZoneWrongStringTest()
    {
        ParamValidator.isTimezoneValid("TestString");
    }
    @Test ( expected = IllegalArgumentException.class)
    public void timeZoneMissingStringTest()
    {
        ParamValidator.isTimezoneValid(null);
    }

    @Test ( expected = Exception.class)
    public void DBinitializedFailedTest() throws Exception
    {
        Utils.isDBInitialized(null);
    }

    @Test
    public void DBinitializedTest() throws Exception
    {
        Assert.assertTrue(Utils.isDBInitialized(Mockito.mock(MemoryDB.class)));
    }

    @Test
    public void StringValidationTest()
    {
        Assert.assertTrue(Utils.isStringValid("test"));
        Assert.assertFalse(Utils.isStringValid(null));
        Assert.assertFalse(Utils.isStringValid(""));
        Assert.assertFalse(Utils.isStringValid(" "));
    }

    @Test
    public void userIdCheckTest()
    {
        Assert.assertTrue(ParamValidator.isUserIdValid("test"));
        Assert.assertFalse(Utils.isStringValid(null));
        Assert.assertFalse(Utils.isStringValid(""));
        Assert.assertFalse(Utils.isStringValid("    "));
    }

    @Test ( expected = Exception.class)
    public void userIdCheckExceptionTest()
    {
        ParamValidator.isUserIdValid(null);
    }

    @Test
    public void countryCodeTest()
    {
        Assert.assertTrue(ParamValidator.isCountryCodeValid("US"));
        Assert.assertTrue(ParamValidator.isCountryCodeValid("SI"));
    }

    @Test ( expected = IllegalArgumentException.class)
    public void countryCodeTooLongStringTest()
    {
        ParamValidator.isCountryCodeValid("xxx");
    }
    @Test ( expected = IllegalArgumentException.class)
    public void countryCodeMissingStringTest()
    {
        ParamValidator.isCountryCodeValid(null);
    }
    @Test ( expected = IllegalArgumentException.class)
    public void countryCodeWrongStringTest()
    {
        ParamValidator.isCountryCodeValid("ĐŠ");
    }

}
