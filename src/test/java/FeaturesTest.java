import com.fun7.app.FeatureRegister;
import com.fun7.features.cs.CustomerSupportFeature;
import com.fun7.features.multiplayer.MultiplayerFeature;
import com.fun7.persistence.IPersistence;
import com.fun7.persistence.impl.MemoryDB;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class FeaturesTest
{
    @Test
    public void customerSupportAvailableTest ()
    {
        CustomerSupportFeature csf = spy(new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE));

        when(csf.getLocalDateTimeNow(ZoneId.of(CustomerSupportFeature.TIME_ZONE_EUROPE_LJUBLJANA))).thenReturn(LocalDate.now().atTime(10, 0));

        Assert.assertTrue(csf.isTimeOfWorkingHours());
    }

    @Test
    public void customerSupportNotAvailableTest ()
    {
        CustomerSupportFeature csf = spy(new CustomerSupportFeature(FeatureRegister.IS_AVAILABLE));

        when(csf.getLocalDateTimeNow(ZoneId.of(CustomerSupportFeature.TIME_ZONE_EUROPE_LJUBLJANA))).thenReturn(LocalDate.now().atTime(16, 0));

        Assert.assertFalse(csf.isTimeOfWorkingHours());
    }

    @Test
    public void mutliplayerPlayGameTest ()
    {
        /* This test will only make sense when more BL is added to mf.playGame() method. */
        IPersistence db = mock(MemoryDB.class);
        MultiplayerFeature mf = spy(new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, db));

        when(db.updateMultiplayerCounter("test1")).thenReturn(1);

        Assert.assertEquals(Integer.valueOf(1), mf.playAGame("test1"));
    }

    @Test
    public void mutliplayerGetNumberOfPlayGamesTest ()
    {
        /* This test will only make sense when more BL is added to mf.getNumberOfGamesPlayed() method. */
        IPersistence db = mock(MemoryDB.class);
        MultiplayerFeature mf = spy(new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, db));

        when(db.getMutliplayerCounter("test1")).thenReturn(5);

        Assert.assertEquals(Integer.valueOf(5), Integer.valueOf(mf.getNumberOfGamesPlayed("test1")));
    }

    @Test
    public void mutliplayerIsUnlockedTest ()
    {
        boolean AVAILABLE = true;
        boolean NOT_AVAILABLE = false;

        IPersistence db = mock(MemoryDB.class);
        MultiplayerFeature mf = spy(new MultiplayerFeature(FeatureRegister.IS_AVAILABLE, db));

        when(mf.getNumberOfGamesPlayed("test1")).thenReturn(6);

        Assert.assertEquals(AVAILABLE, mf.isUnlockedForPlayer("test1", "US"));

        when(mf.getNumberOfGamesPlayed("test1")).thenReturn(5);

        Assert.assertEquals(NOT_AVAILABLE, mf.isUnlockedForPlayer("test1", "US"));

        Assert.assertEquals(NOT_AVAILABLE, mf.isUnlockedForPlayer("test1", "WRONG"));
    }



}
