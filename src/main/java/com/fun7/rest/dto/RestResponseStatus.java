package com.fun7.rest.dto;

/**
 * Interface with responses for REST DTOs.
 */
public interface RestResponseStatus {

    /* Responses catalogue, used in REST calls for status reports about processes on server-side. */
    String RESPONSE_SUCCESS = "Success";
    String RESPONSE_ERROR = "Error";
}
