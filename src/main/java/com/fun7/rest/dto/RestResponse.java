package com.fun7.rest.dto;

import java.io.Serializable;

/**
 * Generic DTO for REST responses.
 */
public final class RestResponse implements Serializable
{

    private static final long serialVersionUID = -9108161550357669863L;

    private String status;
    private String message;

    public RestResponse ()
    {
    }

    public RestResponse (String status, String message)
    {
        this.status = status;
        this.message = message;
    }

    /**
     * Getter for status of response. Preferably use following value: {@link RestResponseStatus#RESPONSE_SUCCESS} or
     * {@link RestResponseStatus#RESPONSE_ERROR}.
     *
     * @return A {@link String} response status value.
     */
    public String getStatus ()
    {
        return status;
    }

    /**
     * Setter for status of response. Preferably use following value: {@link RestResponseStatus#RESPONSE_SUCCESS} or
     * {@link RestResponseStatus#RESPONSE_ERROR}.
     *
     * @param status A {@link String} response status value.
     */
    public void setStatus (String status)
    {
        this.status = status;
    }

    /**
     * Getter for generic response message. Content can be anything, but preferably should be used for content and error
     * descriptions.
     *
     * @return A {@link String} response content.
     */
    public String getMessage ()
    {
        return message;
    }

    /**
     * Setter for generic response message. Content can be anything, but preferably should be used for content and error
     * descriptions.
     *
     * @param message A {@link String} response content.
     */
    public void setMessage (String message)
    {
        this.message = message;
    }
}
