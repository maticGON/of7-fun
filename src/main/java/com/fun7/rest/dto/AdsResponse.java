package com.fun7.rest.dto;

/**
 * REST DTO for {@link com.fun7.features.ads.AdsFeature}. Used for response from 3rd party system which answers
 * if user device is supported or not (for ads).
 */
public class AdsResponse
{
    private String ads;

    /* Possible responses from 3rd party. */
    public static final String RESPONSE_POSITIVE = "sure, why not!";
    public static final String RESPONSE_NEGATIVE = "you shall not pass!";

    public AdsResponse ()
    {
    }

    /**
     * Constructor with all fields to be initialized.
     * @param ads A {@link String} value that describes if ads are supported or not.
     */
    public AdsResponse (String ads)
    {
        this.ads = ads;
    }

    /**
     * Getter for value that describes if ads are supported or not.
     * @return A {@link String} value if ads are supported by 3rd party system or not.
     */
    public String getAds ()
    {
        return ads;
    }

    /**
     * Setter for value that describes if ads are supported or not.
     * @param ads A {@link String} value if ads are supported by 3rd party system or not.
     */
    public void setAds (String ads)
    {
        this.ads = ads;
    }
}
