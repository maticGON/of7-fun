package com.fun7.rest;

import com.fun7.app.FeatureRegister;
import com.fun7.features.IFeature;
import com.fun7.features.ads.IAdsFeature;
import com.fun7.features.cs.ICustomerSupportFeature;
import com.fun7.features.multiplayer.IMultiplayerFeature;
import com.fun7.rest.dto.RestResponse;
import com.fun7.rest.dto.RestResponseStatus;
import com.google.gson.Gson;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Implementation of Fun7Server REST Service.
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class RestService implements IRestEndpoints
{
    /* Constants catalogue */
    public static final String FEATURE_DISABLED = "disabled";
    public static final String FEATURE_ENABLED = "enabled";
    public static final String MSG_ERROR_SOMETHING_WENT_WRONG = "ERROR: Something went wrong!";

    public RestService ()
    {
        System.out.println("RestService construct");//TODO: log
    }

    @Override
    public Response play1Game (String userTimezone, String userId, String countryCode)
    {
        /* Input validators. */
        try
        {
            ParamValidator.isUserIdValid(userId);
            ParamValidator.isCountryCodeValid(countryCode);
        } catch (IllegalArgumentException error)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(generateErrorMessage(error.getMessage())).build();
        }

        /* Feature availability check. */
        IMultiplayerFeature multiplayerFeature =
                (IMultiplayerFeature) FeatureRegister.getInstance().getFeature(IMultiplayerFeature.FEATURE_NAME);
        if (multiplayerFeature == null)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(generateErrorMessage(MSG_ERROR_SOMETHING_WENT_WRONG)).build();
        }

        /* Core business logic*/
        multiplayerFeature.playAGame(userId);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(new RestResponse("game", "Played"))).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    public Response featuresStatus (String userTimezone, String userId, String countryCode)
    {
        /* Input validators. */
        try
        {
            ParamValidator.isTimezoneValid(userTimezone);
            ParamValidator.isUserIdValid(userId);
            ParamValidator.isCountryCodeValid(countryCode);
        } catch (IllegalArgumentException error)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(generateErrorMessage(error.getMessage())).build();
        }

        /* Core business logic*/
        HashMap<String, String> featureNameToAvailabilityMap = new HashMap<>();

        checkMultiplayerAvailability(featureNameToAvailabilityMap, userId, countryCode);
        checkCustomerSupportAvailability(featureNameToAvailabilityMap);
        checksAdsAvailability(featureNameToAvailabilityMap, countryCode);

        return Response.status(Response.Status.OK).entity(new Gson().toJson(featureNameToAvailabilityMap)).build();
    }

    @Override
    public Response hello ()
    {
        return Response.status(Response.Status.OK).entity("hello").build();
    }

    private HashMap<String, String> checksAdsAvailability (HashMap<String, String> featureNameToAvailabilityMap,
                                                           String userCountryCode)
    {
        /* Feature availability check. */
        IFeature adsFeature = FeatureRegister.getInstance().getFeature(IAdsFeature.FEATURE_NAME);
        if (adsFeature == null || !adsFeature.isEnabled())
        {
            featureNameToAvailabilityMap.put(IAdsFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
            return featureNameToAvailabilityMap;
        }

        /* Core business logic*/
        if (((IAdsFeature) adsFeature).isAvailableForCountryCode(userCountryCode))
        {
            featureNameToAvailabilityMap.put(IAdsFeature.FEATURE_NAME_SHORT, FEATURE_ENABLED);
        } else
        {
            featureNameToAvailabilityMap.put(IAdsFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
        }

        return featureNameToAvailabilityMap;
    }

    private HashMap<String, String> checkMultiplayerAvailability (HashMap<String, String> featureNameToAvailabilityMap, String userID, String userCountryCode)
    {
        /* Feature availability check. */
        IFeature multiplayerFeature = FeatureRegister.getInstance().getFeature(IMultiplayerFeature.FEATURE_NAME);
        if (multiplayerFeature == null || !multiplayerFeature.isEnabled())
        {
            featureNameToAvailabilityMap.put(IMultiplayerFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
            return featureNameToAvailabilityMap;
        }

        /* Core business logic*/
        if (((IMultiplayerFeature) multiplayerFeature).isUnlockedForPlayer(userID, userCountryCode))
        {
            featureNameToAvailabilityMap.put(IMultiplayerFeature.FEATURE_NAME_SHORT, FEATURE_ENABLED);
        } else
        {
            featureNameToAvailabilityMap.put(IMultiplayerFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
        }

        return featureNameToAvailabilityMap;
    }

    private HashMap<String, String> checkCustomerSupportAvailability (HashMap<String, String> featureNameToAvailabilityMap)
    {
        /* Feature availability check. */
        IFeature customerSupport = FeatureRegister.getInstance().getFeature(ICustomerSupportFeature.FEATURE_NAME);
        if (customerSupport == null || !customerSupport.isEnabled())
        {
            featureNameToAvailabilityMap.put(ICustomerSupportFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
            return featureNameToAvailabilityMap;
        }

        /* Core business logic*/
        if (((ICustomerSupportFeature) customerSupport).isTimeOfWorkingHours())
        {
            featureNameToAvailabilityMap.put(ICustomerSupportFeature.FEATURE_NAME_SHORT, FEATURE_ENABLED);
        } else
        {
            featureNameToAvailabilityMap.put(ICustomerSupportFeature.FEATURE_NAME_SHORT, FEATURE_DISABLED);
        }
        return featureNameToAvailabilityMap;
    }

    /**
     * Method generates JSON with provided error message.
     * @param errorMessage A {@link String} error message what went wrong.
     * @return a {@link String} generated error JSON message.
     */
    private String generateErrorMessage (String errorMessage)
    {
        return new Gson().toJson(new RestResponse(RestResponseStatus.RESPONSE_ERROR, errorMessage));
    }
}

