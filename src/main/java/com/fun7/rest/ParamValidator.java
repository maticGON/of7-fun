package com.fun7.rest;

import com.fun7.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

/**
 * Helper validator class for validating different inputs.
 */
public class ParamValidator
{

    /* Messages catalogue */
    private static final String MSG_ERROR_PARAM_MISSING = "ERROR: <%s> value is missing!";
    private static final String MSG_ERROR_PARAM_NOT_VALID = "ERROR: <%s> value is not valid!";
    private static final String MSG_ERROR_CC_FORMAT_INVALID =
            "ERROR: Provided CountryCode is in invalid format. Value should be in ISO 3166-1 alpha-2 format.";

    /* Constants catalogue */
    public static final boolean TIMEZONE_VALID = true;
    private static final boolean USERID_VALID = true;
    private static final boolean COUNTRY_CODE_VALID = true;
    private static final Set<String> ccSet;

    static
    {
        Set<String> tempCcSet = new HashSet<>();
        Collections.addAll(tempCcSet, Locale.getISOCountries());
        ccSet = Collections.unmodifiableSet(tempCcSet);
    }

    /**
     * Validator method for county code. Value must be in ISO 3166-1 alpha-2 format. Other formats will not be
     * recognized. Exception is thrown if input value is null, empty, has more then 2 letters or does not match any
     * country code.
     *
     * @param countryCode A {@link String } input value to be validated in format ISO 3166-1 alpha-2.
     * @return A {@link Boolean } flag if input value is valid.
     * @throws IllegalArgumentException exception when input parameter is missing, is empty, has more then 2 letters or
     *                                  does not match any country code.
     */
    public static boolean isCountryCodeValid (String countryCode) throws IllegalArgumentException
    {
        if (!Utils.isStringValid(countryCode))
        {
            throw new IllegalArgumentException(String.format(MSG_ERROR_PARAM_MISSING, "CountryCode"));
        }

        if (countryCode.length() > 2)
        {
            throw new IllegalArgumentException(MSG_ERROR_CC_FORMAT_INVALID);
        }

        if (!ccSet.contains(countryCode))
        {
            throw new IllegalArgumentException(String.format(MSG_ERROR_PARAM_NOT_VALID, "CountryCode"));
        }

        return COUNTRY_CODE_VALID;
    }

    /**
     * Validator method for user id. Exception is thrown when user id does not match validator rules. User Id must not
     * be null or empty.
     *
     * @param userId A {@link String} user identificator to be checked.
     * @return A {@link Boolean } flag if input value is valid.
     * @throws IllegalArgumentException exception when input parameter is missing, is empty.
     */
    public static boolean isUserIdValid (String userId) throws IllegalArgumentException
    {
        if (!Utils.isStringValid(userId))
        {
            throw new IllegalArgumentException(String.format(MSG_ERROR_PARAM_MISSING, "UserID"));
        }
        return USERID_VALID;
    }

    /**
     * Validator method for timezone.  Input value must be in pattern (e.g. "America/New_Yok"). Value checker is case
     * sensitive. Exception is returned when parameter is null, empty or value does not match any Java Timezone ID.
     *
     * @param timezone A {@link String} value of timezone to be checked.
     * @return A {@link Boolean } flag if input value is valid.
     * @throws IllegalArgumentException exception when input parameter is missing, is empty or does not match any Java
     *                                  Timezone ID.
     */
    public static boolean isTimezoneValid (String timezone) throws IllegalArgumentException
    {
        if (!Utils.isStringValid(timezone))
        {
            throw new IllegalArgumentException(String.format(MSG_ERROR_PARAM_MISSING, "Timezone"));
        }

        if (!Arrays.asList(TimeZone.getAvailableIDs()).contains(timezone))
        {
            throw new IllegalArgumentException(String.format(MSG_ERROR_PARAM_NOT_VALID, "Timezone"));
        }

        return TIMEZONE_VALID;
    }
}
