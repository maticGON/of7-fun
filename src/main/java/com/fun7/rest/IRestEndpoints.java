package com.fun7.rest;

import com.fun7.features.IFeature;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST API. List of all available endpoints. Concrete implementation is REST framework depended.
 */
public interface IRestEndpoints
{

    /**
     * Increase counter of played games for provided user. Return positive response while system successfully persisted
     * played game. Otherwise return error (missing parameter, wrong parameter value, feature not running on server-side
     * ...)
     *
     * @param userTimezone A {@link String} user's timezone in following pattern (e.g. "America/New_Yok"). Value checker
     *                     is case sensitive.
     * @param userId       A {@link String} user identification.
     * @param countryCode  A {@link String} user's country code in ISO 3166-1 alpha-2 format. Other formats will not be
     *                     recognized.
     * @return A {@link Response} in JSON with HTTP code. Simple JSON key-pair "game:played" is returned, while any
     * exception occurred {@link com.fun7.rest.dto.RestResponse} object is returned with status value {@link
     * com.fun7.rest.dto.RestResponseStatus#RESPONSE_ERROR} and message with error description (e.g. missing parameter,
     * wrong parameter value).
     */
    @POST
    @Path("/play1Game")
    @Produces(MediaType.APPLICATION_JSON)
    Response play1Game (@QueryParam("timezone") String userTimezone, @QueryParam("userId") String userId,
                        @QueryParam("cc") String countryCode);

    /**
     * Increase counter of played games for provided user. Return positive response while system successfully persisted
     * played game. Otherwise return error (missing parameter, wrong parameter value, feature not running on server-side
     * ...)
     *
     * @param userTimezone A {@link String} user's timezone in following pattern (e.g. "America/New_Yok"). Value checker
     *                     is case sensitive.
     * @param userId       A {@link String} user identification.
     * @param countryCode  A {@link String} user's country code in ISO 3166-1 alpha-2 format. Other formats will not be
     *                     recognized.
     * @return A {@link Response} in JSON with HTTP code. For each server feature JSON key-pair
     * "<feature_name>:<availability>" is returned. Feature names are defined in every feature interfaces and can be
     * fetched with {@link IFeature#getFeatureName()}. Availability values are {@link RestService#FEATURE_ENABLED} and
     * {@link RestService#FEATURE_DISABLED}. While any exception occurred {@link com.fun7.rest.dto.RestResponse} object
     * is returned with status value {@link com.fun7.rest.dto.RestResponseStatus#RESPONSE_ERROR} and message with error
     * description (e.g. missing parameter, wrong parameter value).
     */
    @GET
    @Path("/featuresStatus")
    @Produces(MediaType.APPLICATION_JSON)
    Response featuresStatus (@QueryParam("timezone") String userTimezone, @QueryParam("userId") String userId,
                             @QueryParam("cc") String countryCode);

    /**
     * Test hello service.
     *
     * @return A {@link String} plain text "hello".
     */
    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    Response hello ();
}
