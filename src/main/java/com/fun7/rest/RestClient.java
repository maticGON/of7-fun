package com.fun7.rest;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Rest Client to fetch data from 3rd party system.
 */
public class RestClient
{
    /* Message catalogue. */
    public static final String MSG_ERROR_3RD_PARTY_RETURNED_INVALID_JSON =
            "ERROR: Received invalid JSON in response from 3rd party system! Message: ";
    public static final String MSG_ERROR_3RD_PARTY_UNKNOWN_HTTP_CODE =
            "ERROR: Received unknown status code from 3rd party system! Message: ";
    private static final String MSG_ERROR_400_MISSING_PARAMETERS = "Missing mandatory parameters!";
    private static final String MSG_ERROR_401_UNAUTHORIZED = "Invalid credentials!";
    private static final String MSG_ERROR_500_SERVER_DOWN = "Server is temporarily not available!";

    /**
     * Create a REST client with provided parameters. Client supports Basic Authentication while (both) username and
     * password are provided as parameters. Fetch result and process it by HTTP code. While {@link Response.Status#OK}
     * is received, process message as JSON input. Start as generic and return in desired class that was provided into
     * the method. While HTTP code is same or higher then 300, process message as text and return appropriate
     * exception.
     *
     * @param endpoint      A required {@link String} URI of the endpoint.
     * @param username      An optional {@link java.util.Arrays} of {@link Character} for username of Basic
     *                      Authentication.
     * @param password      An optional {@link java.util.Arrays} of {@link Character} for password of Basic *
     *                      Authentication.
     * @param queryParamMap An optional {@link MultivaluedMap} of key-value pairs for query parameters.
     * @param clasz         A required {@link Class} in which result will be returned to the caller of the method.
     * @return A result of method  in {@link T} class that was provided by caller in method parameters.
     * @throws IllegalArgumentException when HTTP higher then 300 is returned, as object can not be parsed to
     * provided class.
     */
    public <T> T getRestResponse (String endpoint, char[] username, char[] password,
                                  MultivaluedMap<String, Object> queryParamMap, Class<T> clasz) throws IllegalArgumentException
    {
        /* Build REST client. */
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(endpoint).queryParams(queryParamMap);

        /* While username and password for basic authentication are missing, don't us it. While request you are
        trying to call requires it, exception with code 401 will be returned. */
        if (username.length > 0 && password.length > 0)
        {
            /* TODO: WARN! Use of String for password, change method that does not use it. */
            target.register(new BasicAuthentication(String.valueOf(username), String.valueOf(password)));
        }

        /* Get response and process message by HTTP code. */
        Response response = target.request().get();
        String jsonResponse = response.readEntity(String.class);

        if (response.getStatus() == Response.Status.OK.getStatusCode())
        {
            //Read & convert the entity
            T genericResponse;
            try
            {
                genericResponse = new Gson().fromJson(jsonResponse, clasz);
            } catch (JsonSyntaxException e)
            {
                throw new IllegalArgumentException(MSG_ERROR_3RD_PARTY_RETURNED_INVALID_JSON + jsonResponse);
            } finally
            {
                response.close();
                client.close();
            }
            return clasz.cast(genericResponse);

        } else if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode())
        {
            throw new IllegalArgumentException(MSG_ERROR_400_MISSING_PARAMETERS);
        } else if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode())
        {
            throw new IllegalArgumentException(MSG_ERROR_401_UNAUTHORIZED);
        } else if (response.getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
        {
            throw new IllegalArgumentException(MSG_ERROR_500_SERVER_DOWN);
        } else
        {
            throw new IllegalArgumentException(MSG_ERROR_3RD_PARTY_UNKNOWN_HTTP_CODE + jsonResponse);
        }
    }
}
