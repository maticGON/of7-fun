package com.fun7.rest;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Filter for logging REST request or response (body).
 */
@Provider
public class OutgoingFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) throws IOException {

        //TODO: DEBUG log only!
        System.out.println("response entity: " + responseContext.getLocation() + " " + responseContext.getHeaders() + " " + responseContext.getLinks());
        System.out.println("request entity: " + requestContext.getHeaders() + " " + responseContext.getStringHeaders());
    }

}