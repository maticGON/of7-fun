package com.fun7.persistence.impl;

import com.fun7.persistence.IPersistence;

import java.util.HashMap;
import java.util.Map;

/**
 * One of concrete implementations of IPersistence.
 */
public class MemoryDB implements IPersistence {

    /* Constants catalogue. */
    private static final int NO_GAMES = 0;

    /* Variables catalogue */
    private final Map<String, Integer> userIdToMPCounterMap = new HashMap<>();

    @Override
    public Integer updateMultiplayerCounter(String userID) {

        if (!userIdToMPCounterMap.containsKey(userID)) {
            userIdToMPCounterMap.put(userID, 0);
        }
        int mpCount = userIdToMPCounterMap.get(userID);
        mpCount = mpCount + 1;
        userIdToMPCounterMap.put(userID, mpCount);

        return userIdToMPCounterMap.get(userID);
    }

    @Override
    public int getMutliplayerCounter(String userID) {
        if (!userIdToMPCounterMap.containsKey(userID)) {
            return NO_GAMES;
        }
        return userIdToMPCounterMap.get(userID);
    }
}
