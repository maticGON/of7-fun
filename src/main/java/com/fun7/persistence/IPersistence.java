package com.fun7.persistence;

/**
 * Interface for different persistence implementations. Always use this interface when initializing concrete persistence
 * implementations, as it is easier to change it when needed. This way system is not depended on single version or
 * implementation of underlying persistence implementation.
 */
public interface IPersistence
{
    /**
     * Update of played game for provided user.
     *
     * @param userID A required {@link String} user identificator.
     * @return A {@link Integer} number of played game for provided user.
     */
    Integer updateMultiplayerCounter (String userID);

    /**
     * Fetch number of all played games by provided user.
     *
     * @param userID A required {@link String} user identificator.
     * @return A {@link Integer} number of played game for provided user.
     */
    int getMutliplayerCounter (String userID);
}
