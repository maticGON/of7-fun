package com.fun7.features.cp;

import com.fun7.features.AbstractFeature;

/**
 * Concrete Cross promotion feature implementation of {@link com.fun7.features.IFeature} and {@link ICrossPromotionFeature}.
 */
public class CrossPromotionFeature extends AbstractFeature implements ICrossPromotionFeature {
    public CrossPromotionFeature(boolean isAvailable) {
        super(isAvailable,FEATURE_NAME, FEATURE_NAME_SHORT);
    }

    @Override
    public boolean isEnabled() {
        return isAvailable;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        this.isAvailable = isEnabled;
    }
}
