package com.fun7.features.cp;

/**
 * Interface for Cross Promotion feature.
 */
public interface ICrossPromotionFeature
{
    /* Constants catalogue. */
    String FEATURE_NAME = "CrossPromotionFeature";
    String FEATURE_NAME_SHORT = "cross-promo";
}
