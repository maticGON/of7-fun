package com.fun7.features;

/**
 * Abstract feature based on {@link IFeature} and to be implemented by specific feature.
 */
public abstract class AbstractFeature implements IFeature{

    /* Variables catalogue. */
    protected boolean isAvailable = false;
    private final String featureName;
    private final String featureNameShort;

    public AbstractFeature(boolean isAvailable, String featureName, String featureNameShort) {
        this.isAvailable = isAvailable;
        this.featureName = featureName;
        this.featureNameShort = featureNameShort;
    }

    @Override
    public String getFeatureName() {
        return featureName;
    }

    @Override
    public String getFeatureNameShort() {
        return featureNameShort;
    }
}
