package com.fun7.features;

/**
 * Interface for each feature and all methods that must be implemented by it.
 */
public interface IFeature
{
    /**
     * Getter for full feature name. Feature can also be searched by it in {@link com.fun7.app.FeatureRegister} and be
     * used globally over the project
     *
     * @return A {@link String} full feature name defined in each of the feature interfaces.
     */
    String getFeatureName ();

    /**
     * Getter for short feature name. Used when full name is not needed or is different name is required for feature
     * description (e.g. shorter naming in REST responses, adjusted to customer needs).
     *
     * @return A {@link String} short feature name defined in each of the feature interfaces.
     */
    String getFeatureNameShort ();

    /**
     * Getter for flag if feature is enabled by the system (for all users). Used for e.g. maintenance mode.
     *
     * @return A {@link Boolean} flag with values {@link com.fun7.app.FeatureRegister#IS_AVAILABLE} or {@link
     * com.fun7.app.FeatureRegister#NOT_AVAILABLE} or
     */
    boolean isEnabled ();

    /**
     * Set Flag if feature is enabled by the system (for all users). Used for e.g. maintenance mode.
     *
     * @param isEnabled A {@link Boolean} flag with values {@link com.fun7.app.FeatureRegister#IS_AVAILABLE} or {@link
     *                  com.fun7.app.FeatureRegister#NOT_AVAILABLE} or
     */
    void setEnabled (boolean isEnabled);
}
