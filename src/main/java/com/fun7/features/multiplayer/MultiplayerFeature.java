package com.fun7.features.multiplayer;

import com.fun7.Utils;
import com.fun7.features.AbstractFeature;
import com.fun7.persistence.IPersistence;

import java.util.Locale;

/**
 * Concrete Multiplayer feature implementation  of {@link com.fun7.features.IFeature} and {@link IMultiplayerFeature}.
 */
public class MultiplayerFeature extends AbstractFeature implements IMultiplayerFeature
{
    /* Constants catalogue. */
    private static final int GAMES_PLAYED_MP_UNLOCK_LIMIT = 5;//TODO: move to configuration
    private static final boolean FEATURE_NOT_AVAILABLE_FOR_USER = false;
    private static final boolean FEATURE_AVAILABLE_FOR_USER = true;

    /* Variables catalogue. */
    IPersistence db;

    public MultiplayerFeature (boolean isAvailable, IPersistence db)
    {
        super(isAvailable, FEATURE_NAME, FEATURE_NAME_SHORT);

        try
        {
            Utils.isDBInitialized(db);
            this.db = db;
        } catch (Exception e)
        {
            //TODO: log
            System.out.println("ERROR: DB!");
        }
    }

    @Override
    public boolean isEnabled ()
    {
        return isAvailable;
    }

    @Override
    public void setEnabled (boolean isEnabled)
    {
        this.isAvailable = isEnabled;
    }

    @Override
    public Integer playAGame (String userId)
    {
        return db.updateMultiplayerCounter(userId);
    }

    @Override
    public int getNumberOfGamesPlayed (String userId)
    {
        return db.getMutliplayerCounter(userId);
    }

    @Override
    public boolean isUnlockedForPlayer (String userID, String countryCode)
    {
        if (!new Locale("", countryCode).getCountry().equals(Locale.US.getCountry()))
        {
            return FEATURE_NOT_AVAILABLE_FOR_USER;
        }

        if (getNumberOfGamesPlayed(userID) <= GAMES_PLAYED_MP_UNLOCK_LIMIT)
        {
            return FEATURE_NOT_AVAILABLE_FOR_USER;
        }

        return FEATURE_AVAILABLE_FOR_USER;
    }
}
