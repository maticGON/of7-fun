package com.fun7.features.multiplayer;

/**
 * Interface for multiplayer feature.
 */
public interface IMultiplayerFeature
{
    /* Constants catalogue. */
    String FEATURE_NAME = "MutliplayerFeature";
    String FEATURE_NAME_SHORT = "mutliplayer";

    /**
     * Play single game. Increases counter in DB by 1.
     *
     * @param userId A {@link String } userId which played game.
     * @return A {@link Integer} number of played game for provided user.
     */
    Integer playAGame (String userId);

    /**
     * Return number of played games for provided userId.
     *
     * @param userId A {@link String } userId we requested number of played games for.
     * @return A {@link Integer} number of played games for provided userId.
     */
    int getNumberOfGamesPlayed (String userId);

    /**
     * Returns flag if user has already unlocked multiplayer. Multiplayer unlocks when user reached enough played games
     * and is from region where multiplayer is available.
     *
     * @param userId      A {@link String } user identificator.
     * @param countryCode A {@link String } input value to be validated in format ISO 3166-1 alpha-2.
     * @return A {@link Boolean} flag if mutliplayer if available for provided user.
     */
    boolean isUnlockedForPlayer (String userId, String countryCode);

}
