package com.fun7.features.cs;

import com.fun7.features.AbstractFeature;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * Concrete Customer support feature implementation of {@link com.fun7.features.IFeature} and {@link ICustomerSupportFeature}.
 */
//TODO: Check which feature naming is correct: customer-support or user-support, as documentation uses them for same
// thing.
public class CustomerSupportFeature extends AbstractFeature implements ICustomerSupportFeature
{

    public CustomerSupportFeature (boolean isAvailable)
    {
        super(isAvailable, FEATURE_NAME, FEATURE_NAME_SHORT);
    }

    @Override
    public boolean isEnabled ()
    {
        return isAvailable;
    }

    @Override
    public void setEnabled (boolean isEnabled)
    {
        this.isAvailable = isEnabled;
    }

    @Override
    public boolean isTimeOfWorkingHours ()
    {
        ZoneId zoneId = ZoneId.of(TIME_ZONE_EUROPE_LJUBLJANA);
        LocalTime startTime = LocalTime.of(WORK_HOURS_START_HOURS, WORK_HOURS_START_MINUTES);
        LocalTime endTime = LocalTime.of(WORK_HOURS_END_HOURS, WORK_HOURS_END_MINUTES);
        LocalDateTime currentDateTime = getLocalDateTimeNow(zoneId);
        if (currentDateTime.toLocalTime().isAfter(startTime) && currentDateTime.toLocalTime().isBefore(endTime))
        {
            return WORKING_HOURS_OPEN;
        } else
        {
            return WORKING_HOURS_CLOSED;
        }
    }

    /**
     * Helper method that return LocalDateTime.now() for provided timezone. Method is extracted for easier unit testing.
     * @param zoneId A {@link ZoneId} timezone.
     * @return Current {@link LocalDateTime} for provided timezone.
     */
    public LocalDateTime getLocalDateTimeNow (ZoneId zoneId)
    {
        return LocalDateTime.now(zoneId);
    }
}
