package com.fun7.features.cs;

/**
 * Interface for Customer Support feature.
 */
public interface ICustomerSupportFeature
{
    /* Constants catalogue. */
    String FEATURE_NAME = "CustomerSupportFeature";
    String FEATURE_NAME_SHORT = "user-support"; /* WARN!!! By documentation json name and feature name are different! */

    String TIME_ZONE_EUROPE_LJUBLJANA = "Europe/Ljubljana";
    int WORK_HOURS_START_HOURS = 9;
    int WORK_HOURS_START_MINUTES = 0;
    int WORK_HOURS_END_HOURS = 15;
    int WORK_HOURS_END_MINUTES = 0;

    boolean WORKING_HOURS_OPEN = true;
    boolean WORKING_HOURS_CLOSED = false;

    /**
     * Return flag if customer support is available at current time. Timezone and work hours are provided by interface.
     *
     * @return A {@link Boolean} flag if Customer support is available at current time.
     */
    boolean isTimeOfWorkingHours ();
}
