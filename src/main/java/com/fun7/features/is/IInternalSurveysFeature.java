package com.fun7.features.is;

/**
 * Interface for Internal Surveys feature.
 */
public interface IInternalSurveysFeature
{

    /* Constants catalogue. */
    String FEATURE_NAME = "InternalSurveysFeature";
    String FEATURE_NAME_SHORT = "internal-surveys";

}
