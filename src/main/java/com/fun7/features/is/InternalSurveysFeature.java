package com.fun7.features.is;

import com.fun7.features.AbstractFeature;

/**
 * Concrete Internal surveys feature implementation  of {@link com.fun7.features.IFeature} and {@link IInternalSurveysFeature}.
 */
public class InternalSurveysFeature extends AbstractFeature implements IInternalSurveysFeature {
    public InternalSurveysFeature(boolean isAvailable) {
        super(isAvailable, FEATURE_NAME, FEATURE_NAME_SHORT);
    }

    @Override
    public boolean isEnabled() {
        return isAvailable;
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        this.isAvailable = isEnabled;
    }
}
