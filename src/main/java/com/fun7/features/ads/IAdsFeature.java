package com.fun7.features.ads;

/**
 * Interface for Ads feature.
 */
public interface IAdsFeature
{
    /* Contants catalogue. */
    String FEATURE_NAME = "AdsFeature";
    String FEATURE_NAME_SHORT = "ads";

    /**
     * Returns flag if provided country code is supported by 3rd party system to have ads shown.
     *
     * @param countryCode A {@link String } input value to be validated in format ISO 3166-1 alpha-2.
     * @return A {@link Boolean} flag if country code is supported by 3rd party system to have ads shown.
     */
    boolean isAvailableForCountryCode (String countryCode);
}
