package com.fun7.features.ads;

import com.fun7.features.AbstractFeature;
import com.fun7.rest.RestClient;
import com.fun7.rest.dto.AdsResponse;

import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Concrete Ads feature implementation of {@link com.fun7.features.IFeature} and {@link IAdsFeature}.
 */
/*
 * TODO: Username, password and endpoint can be added to configuration file
 * */
public class AdsFeature extends AbstractFeature implements IAdsFeature
{
    /* Constants catalogue. */
    public static final char[] BASIC_AUTH_USERNAME = new char[]{'f', 'u', 'n', '7', 'u', 's', 'e', 'r'};
    public static final char[] BASIC_AUTH_PASSWORD = new char[]{'f', 'u', 'n', '7', 'p', 'a', 's', 's'};
    private static final String ENDPOINT_SERVER_ADS = "https://us-central1-o7tools.cloudfunctions.net/fun7-ad-partner?";
    public static final String QUERY_PARAM_COUNTRY_CODE = "countryCode";
    public static final boolean FEATURE_AVAILABLE = true;
    public static final boolean FEATURE_NOT_AVAILABLE = false;

    /* Variables catalogue. */
    RestClient restClient;

    public AdsFeature (boolean isAvailable)
    {
        super(isAvailable, IAdsFeature.FEATURE_NAME, FEATURE_NAME_SHORT);
        restClient = new RestClient();
    }

    @Override
    public boolean isEnabled ()
    {
        return isAvailable;
    }

    @Override
    public void setEnabled (boolean isEnabled)
    {
        this.isAvailable = isEnabled;
    }

    @Override
    public boolean isAvailableForCountryCode (String countryCode)
    {
        MultivaluedMap<String, Object> queryParamMap = new MultivaluedMapImpl<>();
        queryParamMap.add(QUERY_PARAM_COUNTRY_CODE, countryCode);

        AdsResponse adsResponse;
        try
        {
            adsResponse = (AdsResponse) restClient.getRestResponse(ENDPOINT_SERVER_ADS, BASIC_AUTH_USERNAME,
                    BASIC_AUTH_PASSWORD, queryParamMap, AdsResponse.class);
        } catch (IllegalArgumentException e)
        {
            return FEATURE_NOT_AVAILABLE;
        }

        switch (adsResponse.getAds())
        {
            case AdsResponse.RESPONSE_POSITIVE:
                return FEATURE_AVAILABLE;
            case AdsResponse.RESPONSE_NEGATIVE:
            default:
                /*Show feature as disables, while (possibly) error has been received or something updated on 3rd
                party system.*/
                return FEATURE_NOT_AVAILABLE;
        }
    }
}
