package com.fun7.features.iap;

/**
 * Interface for IAP feature.
 */
public interface IIAPFeature
{
    /* Constants catalogue. */
    String FEATURE_NAME = "IAPFeature";
    String FEATURE_NAME_SHORT = "iap";

}
