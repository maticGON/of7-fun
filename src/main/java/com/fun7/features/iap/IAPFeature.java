package com.fun7.features.iap;

import com.fun7.features.AbstractFeature;

/**
 * Concrete IAP feature implementation  of {@link com.fun7.features.IFeature} and {@link IIAPFeature}.
 */
public class IAPFeature extends AbstractFeature implements IIAPFeature
{

    public IAPFeature (boolean isAvailable)
    {
        super(isAvailable, FEATURE_NAME, FEATURE_NAME_SHORT);
    }

    @Override
    public boolean isEnabled ()
    {
        return isAvailable;
    }

    @Override
    public void setEnabled (boolean isEnabled)
    {
        this.isAvailable = isEnabled;
    }
}
