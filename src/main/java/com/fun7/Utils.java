package com.fun7;

import com.fun7.persistence.IPersistence;

/**
 * Utils class with helper methods available in whole project.
 */
public class Utils
{
    /*Message catalogue.*/
    private static final String MSG_ERROR_PERSISTENCE_NOT_AVAILABLE = "Persistence not available!";

    /* Constants catalogue. */
    private static final boolean DB_INITIALIZED = true;

    /**
     * Validator method for checking if provided string value is null or empty.
     *
     * @param text A {@link String} value to be checked.
     * @return A {@link Boolean} flag if string is valid or not.
     */
    public static boolean isStringValid (String text)
    {
        return text != null && !text.trim().isEmpty();
    }

    /**
     * Validator method to check if database is already initialized and is accessible for use.
     *
     * @param db A {@link IPersistence} interface that every concrete persistence implementation implements.
     * @return A {@link Boolean} flag if DB is ready for use.
     * @throws Exception when provided DB is not ready for use.
     */
    public static boolean isDBInitialized (IPersistence db) throws Exception
    {
        if (db == null)
        {
            //TODO: LOG error -> no persistence
            throw new Exception(MSG_ERROR_PERSISTENCE_NOT_AVAILABLE);
        }
        return DB_INITIALIZED;

    }
}
