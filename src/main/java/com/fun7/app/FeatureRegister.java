package com.fun7.app;

import com.fun7.features.IFeature;
import com.fun7.persistence.IPersistence;

import java.util.HashSet;
import java.util.Set;

/**
 * Register of singletons available everywhere in project. Register can accept any class, but is main purpose is to
 * handle concrete implementation of {@link IPersistence} persistence and list of {@link IFeature} features.
 */
public final class FeatureRegister
{
    /* Constants catalogue*/
    public static final boolean IS_AVAILABLE = true;
    public static final boolean NOT_AVAILABLE = false;
    private static final IFeature FEATURE_NOT_ENABLED = null;
    private static final IPersistence PERSISTENCE_NOT_AVAILABLE = null;
    private static final String MSG_ERR_PERSISTENCE_ALREADY_REGISTERED = "ERROR: Concrete class of persistence has " +
            "already registered! System can not provide 2 different persistences.";

    /* Singleton instance */
    private static final FeatureRegister INSTANCE = new FeatureRegister();

    /* Variables */
    private final Set<Object> singletons = new HashSet<>();

    /**
     * Getter for singleton to return saved singletons of registered features.
     *
     * @return {@link FeatureRegister} register instance
     */
    public static FeatureRegister getInstance ()
    {
        return INSTANCE;
    }

    /**
     * Add new singleton to list of all features. Can be any class, but preferred is concrete implementation of {@link
     * IPersistence} persistence and list of {@link IFeature} features.
     *
     * @param singleton A {@link Object} or can be any class, but preferred is concrete implementation of {@link
     *      IPersistence} persistence or {@link IFeature} features.
     * @throws Exception when trying to register second persistence.
     */
    public void addSingleton (Object singleton) throws Exception
    {
        if (singleton instanceof IPersistence && getPersistence() != null)
        {
            throw new Exception(MSG_ERR_PERSISTENCE_ALREADY_REGISTERED);
        }
        singletons.add(singleton);
    }

    /**
     * Get {@link IFeature} feature by name. While feature with provided name is not found {@code null is returned.}
     *
     * @param featureName A {@link String} name of feature that is required to be set by {@link
     *                    IFeature#getFeatureName()}.
     * @return A {@link IFeature} feature implementation matching provided feature name.
     */
    public IFeature getFeature (String featureName)
    {
        for (Object item : singletons)
        {
            if (item instanceof IFeature && ((IFeature) item).getFeatureName().equals(featureName))
            {
                return (IFeature) item;
            }
        }

        return FEATURE_NOT_ENABLED;
    }

    /**
     * Get the only {@link IPersistence} persistence system is running on. While no persistence has been registered yet
     * {@code null} is returned.
     *
     * @return A {@link IPersistence} implementation of persistence.
     */
    public IPersistence getPersistence ()
    {
        for (Object item : singletons)
        {
            if (item instanceof IPersistence)
            {
                return (IPersistence) item;
            }
        }

        return PERSISTENCE_NOT_AVAILABLE;
    }

    /**
     * Unregister singleton and take access to it.
     * @param singleton A {@link Object } one of concrete implementations of {@link IFeature }, {@link IPersistence} or something else.
     */
    public void unregister(Object singleton)
    {
        singletons.remove(singleton);
    }

    /**
     * Returns set of all registered singletons.
     * @return a {@link Set} of all registered. singletons.
     */
    public Set<Object> getSingletons()
    {
        return singletons;
    }

}
