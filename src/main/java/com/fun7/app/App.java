package com.fun7.app;

import com.fun7.features.IFeature;
import com.fun7.features.ads.AdsFeature;
import com.fun7.features.cs.CustomerSupportFeature;
import com.fun7.features.multiplayer.MultiplayerFeature;
import com.fun7.persistence.IPersistence;
import com.fun7.persistence.impl.MemoryDB;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static com.fun7.app.FeatureRegister.IS_AVAILABLE;

/**
 * Main starting class to initialize different features of system.
 */
public class App implements ServletContextListener
{

    @Override
    public void contextDestroyed (ServletContextEvent arg0)
    {
        //Notification that the servlet context is about to be shut down.
    }

    @Override
    public void contextInitialized (ServletContextEvent arg0)
    {

        System.out.println("fun7 construct");//TODO:Log

        /* Persistence & features register section -> concrete implementation can be changed and can be read from
        configuration in future upgrades.*/
        IPersistence db = new MemoryDB();

        IFeature multiplayerFeature = new MultiplayerFeature(IS_AVAILABLE, db);
        IFeature customerSupportFeature = new CustomerSupportFeature(IS_AVAILABLE);
        IFeature adsFeature = new AdsFeature(IS_AVAILABLE);

        try
        {
            FeatureRegister.getInstance().addSingleton(db);

            FeatureRegister.getInstance().addSingleton(multiplayerFeature);
            FeatureRegister.getInstance().addSingleton(customerSupportFeature);
            FeatureRegister.getInstance().addSingleton(adsFeature);

        } catch (Exception e)
        {
            System.out.println("ERROR: startup" + e.getMessage());
            //TODO: log
        }
    }
}
