package com.fun7.app;

import com.fun7.rest.OutgoingFilter;
import com.fun7.rest.RestService;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

/**
 * JAX-RS compatible {@link Application} which will allow for initialisation of the JAX-RS (RESTEasy) libraries.
 */
public class Fun7Application extends Application
{
    /* Variables catalogue. */
    private final Set<Object> singletons = new HashSet<>();

    /**
     * Default constructor to register concrete REST resource implementation. Additionally add filters.
     */
    public Fun7Application ()
    {
        // Register REST service
        System.out.println("rest resource");//TODO:Log
        singletons.add(new RestService());
        singletons.add(new OutgoingFilter());
    }

    @Override
    public Set<Object> getSingletons ()
    {
        return singletons;
    }
}